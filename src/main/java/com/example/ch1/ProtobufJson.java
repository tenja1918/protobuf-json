package com.example.ch1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.springframework.util.ResourceUtils;

import com.example.tutorial.AddressBookProtos;
import com.google.protobuf.util.JsonFormat;

/**
 * address_book.protoに従ったフォーマットのjsonファイルを読み込む。JavaObject  <--->  JSON Stringの相互変換サンプル。
 */
public class ProtobufJson {

	public static void main(String[] args) throws Exception {
		File jsonFile = ResourceUtils.getFile("classpath:json/address_book.json");

		System.out.println("----------------------------------------------------");
		System.out.println("address_book.json  --->  AddressBook JavaObject");
		System.out.println("----------------------------------------------------");
		AddressBookProtos.AddressBook book = getAddressBook(jsonFile);
		System.out.println(book);

		System.out.println("----------------------------------------------------");
		System.out.println("AddressBook JavaObject  --->  JSON String");
		System.out.println("----------------------------------------------------");
		String json = JsonFormat.printer().print(book);
		System.out.println(json);
	}

	public static AddressBookProtos.AddressBook getAddressBook(File jsonFile) throws FileNotFoundException, IOException {
		try (Reader reader = new FileReader(jsonFile)) {

			AddressBookProtos.AddressBook.Builder builder = AddressBookProtos.AddressBook.newBuilder();
			JsonFormat.parser().merge(reader, builder);
			return builder.build();
		}
	}
}
