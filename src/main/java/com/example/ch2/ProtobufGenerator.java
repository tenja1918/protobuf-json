package com.example.ch2;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.DescriptorProtos.FileDescriptorProto;
import com.google.protobuf.compiler.PluginProtos;

/**
 * protoファイルを解析したprotocの結果を受け取り、.dumpファイルとして内容をそのまま出力するprotoc-plugin。
 * 下記図のpluginに当たる。
 *
 * file.proto  ----->  protoc  ----->  plugin-generaged code
 *
 *                      |  ^
 *                      |  |
 *                      |  |
 *                      v  |
 *
 *                     plugin
 */
public class ProtobufGenerator {

	private ProtobufGenerator() {
	}

	/**
	 * protocからの解析結果を受け取り、コード生成リクエストとして返す。
	 * @param is
	 * @return PluginProtos.CodeGeneratorRequest
	 * @throws IOException
	 */
	public static PluginProtos.CodeGeneratorRequest parse(InputStream is) throws IOException {
		return PluginProtos.CodeGeneratorRequest.parseFrom(is);
	}

	/**
	 * コード生成リクエストに応じてプラグイン固有の処理を実施する。
	 * 結果をPluginProtos.CodeGeneratorResponseとして返す。
	 * @param request
	 * @return PluginProtos.CodeGeneratorResponse
	 */
	public static PluginProtos.CodeGeneratorResponse process(PluginProtos.CodeGeneratorRequest request) {
		// .protoファイルごとのマップにする
		Map<String, DescriptorProtos.FileDescriptorProto> files = new HashMap<>();
		for (FileDescriptorProto descriptor : request.getProtoFileList()) {
			files.put(descriptor.getName(), descriptor);
		}

		// .protoファイルごとに処理を行い、出力する
		// ここでは複数の入力に対してそれぞれ出力ファイルを生成しているが、出力は1つにしてもよい。プラグインに委ねられている
		PluginProtos.CodeGeneratorResponse.Builder builder = PluginProtos.CodeGeneratorResponse.newBuilder();
		for (String fname : request.getFileToGenerateList()) {
			FileDescriptorProto descriptor = files.get(fname);

			PluginProtos.CodeGeneratorResponse.File.Builder fileBuilder =
					PluginProtos.CodeGeneratorResponse.File.newBuilder();
			// 出力ファイル名
			fileBuilder.setName(fname + ".dump");
			// 出力内容
			fileBuilder.setContentBytes(descriptor.toByteString());
			// 以下のように文字列を書き込むこともできる
			//fileBuilder.setContent("abababa");

			builder.addFile(fileBuilder.build());
		}

		return builder.build();
	}

	/**
	 * プラグインの処理結果を出力する。protocはこの内容にしたがってファイル/ディレクトリを生成する
	 * @param response
	 * @throws IOException
	 */
	public static void emit(PluginProtos.CodeGeneratorResponse response) throws IOException {
		response.writeTo(System.out);
	}

	public static void main(String[] args) throws Exception {
		// request
		PluginProtos.CodeGeneratorRequest request;
		if (args.length > 0) {
			// pluginをデバッグしたい場合は、ProtobufInputの出力結果を引数にして実行する
			try (InputStream is = new FileInputStream(args[0])) {
				request = parse(is);
			}
		} else {
			request = parse(System.in);
		}

		// response
		PluginProtos.CodeGeneratorResponse response = process(request);

		// output
		emit(response);
	}
}
