package com.example.ch2;

import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

/**
 * 下記図の * に当たる部分の内容を出力する。
 * 出力したファイルは、protoc pluginのデバッグに用いることができる。
 *
 * file.proto  ----->  protoc  ----->  plugin-generaged code
 *
 *                      |  ^
 *                      *  |
 *                      |  |
 *                      v  |
 *
 *                     plugin
 */
public class ProtobufInput {
	public static void main(String[] args) throws Exception {
		String outFilename = args.length > 0 ? args[0] : "protoc-plugin-input.dump";
		try (OutputStream os = new FileOutputStream(outFilename)) {
			IOUtils.copy(System.in, os);
		}
	}
}
