# protobuf-json

Protocol Buffers のJavaサンプルです。

Windows環境を前提しています。その他の環境の場合、適宜読み替えてください。

サンプルとして以下の.protoにてスキーマを定義、そのスキーマにしたがった.jsonがあるとします。（リポジトリに含まれています）

1. .protoファイルを作成し、スキーマを定義する（resource/protobuf/address_book.proto）
1. .protoファイルで定義したスキーマにしたがった.jsonファイルを作成する（resources/json/address_book.json）


## protocのインストール

.protocをコンパイルするためのツールをダウンロードします。（protoc）

1. protocをダウンロードします。（https://github.com/protocolbuffers/protobuf/releases ）ReleaseのAssetsから探してください。
1. Windows 64bitの場合、protoc-x.x.x-win64.zip等になります。
1. 解凍するとproto.exeがありますので、適当なフォルダに配置します。
1. protoc.exeのパスを環境変数に設定します。


## .protoファイルに対応するJavaクラスソースコードの生成サンプル

.protoファイルに対応するJavaクラスソースコードを生成するサンプルです。

1. java.exeのパスを環境変数に設定します。
1. コマンドプロンプトを開き、次のbatファイルの場所まで移動します。（resources/protoexec/gen-java.bat）
1. batファイルを実行してください。（resources/protoexec/gen-java.bat）
1. AddressBookProtos.javaが作成されます。（java/com/example/tutorial/AddressBookProtos.java）


## JSONとJavaオブジェクトの相互変換サンプル

resources/json/address_book.jsonを読み込み、相互に変換するサンプルです。（ProtobufJson.java）

IDEやコマンドラインで、直接Javaアプリケーションとして実行します。


## プラグインサンプル

protocプラグインのサンプルです。

以下の記事を参考にJavaで実装したものになります。（Javaの実装に問題があればtenja1918に責があります）
https://qiita.com/yugui/items/87d00d77dee159e74886

1. java.exeのパスを環境変数に設定します。
1. コマンドプロンプトを開き、次のbatファイルの場所まで移動します。（resources/protoexec/exec-plugin.bat）
1. batファイルを実行してください。（resources/protoexec/exec-plugin.bat）
1. proto-gen-foo.batが実行され、ProtobufGeneratorが呼ばれます。（resources/protoexec/proto-gen-foo.bat）
