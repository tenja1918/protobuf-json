@echo off
: プラグインの入力内容をそのままダンプする。exec-plugin.batからprotocを通して呼び出される。直接呼び出すものではない。
: 事前にjava.exeにパスを通してください。

java -cp ".;..\..\..\..\target\classes;%userprofile%\.m2\repository\commons-io\commons-io\2.6\commons-io-2.6.jar;%userprofile%\.m2\repository\org\apache\commons\commons-lang3\3.9\commons-lang3-3.9.jar;%userprofile%\.m2\repository\com\google\protobuf\protobuf-java\3.8.0\protobuf-java-3.8.0.jar" com.example.ch2.ProtobufInput
